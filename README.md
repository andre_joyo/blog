# Blog a medida

Sencillo blog programado a medida.

## Empezando

Para iniciar el proyecto debes descargarlo en la carpeta apropiada del servidor local que tengas en tu dispossitivo el repositorio y ejecutar el index.php en un navegador moderno.

### Prerrequisitos

Debes tener instalado un paquete como xampp , wamp, etc.

## Built With

* [xampp](https://www.apachefriends.org/es/index.html) - The web package used

## Author

* **Andr� Joyo** 

## License

This project has not license
