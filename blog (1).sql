-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-01-2020 a las 13:15:00
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blog`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, '1'),
(2, 'cat2'),
(3, 'categorÃ­a 3'),
(4, 'cat4'),
(5, 'cat25'),
(7, 'cat7'),
(8, 'nueva categorÃ­a'),
(9, 'una categorÃ­a nueva');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `status` enum('private','public') DEFAULT 'public'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comment`
--

INSERT INTO `comment` (`id`, `name`, `comment`, `email`, `post_id`, `created_at`, `status`) VALUES
(1, 'depn', 'npoendpo ponpn opnp onf pnrp', 'wmp@onis.dp', 28, NULL, 'public'),
(2, 'onp', 'pnonpnp  opj jpe doj', 'opwj @nwpds.cnio', 22, NULL, 'public'),
(3, 'onp', 'pnonpnp  opj jpe doj', 'opwj @nwpds.cnio', 28, NULL, 'public'),
(9, NULL, 'edompmdepom coommment', 'eodn@oide', 28, '2020-01-17 11:17:46', 'public'),
(10, NULL, 'un nuevo comentario para este post \'32\'', 'no3in@oned.edio', 32, '2020-01-17 11:52:20', 'public');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `brief` varchar(511) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `status` enum('eraser','to_publish') DEFAULT 'eraser',
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `post`
--

INSERT INTO `post` (`id`, `title`, `brief`, `content`, `image`, `created_at`, `status`, `user_id`, `category_id`) VALUES
(16, 'titulo1', 'breve1 breve1 breve1 breve1 breve1 breve1 breve1', 'contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1 contenido1', NULL, '2020-01-15 06:37:35', 'eraser', 1, 1),
(17, 'titulo del post 17', 'breve resumen del post 17', 'noediom odieop edoopm edomoedm oiednoin denop', NULL, '2020-01-15 06:40:43', 'eraser', 1, 1),
(18, 'titulo de post', 'El pÃ¡rrafo estÃ¡ constituido por una oraciÃ³n principal que puede ser distinguida fÃ¡cilmente, ya que enuncia la parte esencial de la cual dependen los demÃ¡s. Es posible decir entonces que la oraciÃ³n principal posee un sentido esencial del pÃ¡rrafo. La oraciÃ³n principal puede aparecer en el texto de forma implÃ­cita o explÃ­cita. Cuando la oraciÃ³n principal estÃ¡ implÃ­cita, Ã©sta no aparece por escrito en el pÃ¡rrafo y es necesario deducirla. En cambio, la explÃ­cita, sÃ­ la encontramos escrita y pod', 'Y, viÃ©ndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: SÃ¡bete, Sancho, que no es un hombre mÃ¡s que otro si no hace mÃ¡s que otro. Todas estas borrascas que nos suceden son seÃ±ales de que presto ha de serenar el tiempo y han de sucedernos bien las cosas; porque no es posible que el mal ni el bien sean durables, y de aquÃ­ se sigue que, habiendo durado mucho el mal, el bien estÃ¡ ya cerca. AsÃ­ que, no debes congojarte por las desgracias que a mÃ­ me suceden, pues a ti no te cabe parte dellas.Y, viÃ©ndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: SÃ¡bete, Sancho, que no es un hombre mÃ¡s que otro si no hace mÃ¡s que otro. Todas estas borrascas que nos suceden son seÃ±ales de que presto ha de serenar el tiempo y han de sucedernos bien las cosas; porque no es posible que el mal ni el bien sean durables, y de aquÃ­ se sigue que, habiendo durado mucho el mal, el bien estÃ¡ ya cerca. AsÃ­ que, no debes congojarte por las desgracias que a mÃ­ me suceden, pues a ti no', NULL, '2020-01-15 06:41:01', 'eraser', 1, 1),
(19, 'TÃ­tulo del post', 'Breve resumen del post', 'Fragmento de un escrito con unidad temÃ¡tica, que queda diferenciado del resto de fragmentos por un punto y aparte y generalmente tambiÃ©n por llevar letra mayÃºscula inicial y un espacio en blanco en el margen izquierdo de alineaciÃ³n del texto principal de la primera lÃ­nea.', NULL, '2020-01-15 06:42:25', 'to_publish', 1, 1),
(21, 'mi post', 'Breve resumen de mi post', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,', NULL, '2020-01-15 06:45:13', 'eraser', 1, 1),
(22, 'otro post', 'Breve resumen de otro post', 'Un pÃ¡rrafo, tambiÃ©n llamado parÃ¡grafo, es una unidad comunicativa formada por un conjunto de oraciones secuenciales que trata un mismo tema. EstÃ¡ compuesto por un conjunto de oraciones que tienen cierta unidad temÃ¡tica o que, sin tenerla, se enuncian juntas.', NULL, '2020-01-15 06:46:42', 'eraser', 1, 1),
(23, 'titulo3', 'breve3', 'contenido3 edonpe opedpp pedm`pem pmde`p pedm', NULL, '2020-01-15 06:47:29', 'eraser', 1, 1),
(24, 'titulo8', 'Pueden ser de dos tipos: de coordinaciÃ³n y subordinaciÃ³n. Son coordinadas aquellas que estÃ¡n unidas mediante conjunciones y posee en sÃ­ mismo un sentido completo. Son subordinadas aquellas que solo adquieren sentido en funciÃ³n de otra.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2020-01-15 06:47:50', 'to_publish', 1, 1),
(25, 'Perspiciatis', 'Todo pÃ¡rrafo se caracteriza por ser dueÃ±o de un conjunto de oraciones que concatenadas a travÃ©s de la unidad y la coherencia, proyectan una idea sÃ³lida y consumada que cumple asÃ­ la exigencia entre lo que pensamos, lo que queremos decir, y lo que realmente decimos.1â€‹', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere', NULL, '2020-01-15 06:48:09', 'eraser', 1, 1),
(27, 'nuevo', 'nuevo', 'nuevo', NULL, '2020-01-16 11:49:05', 'eraser', 1, 4),
(28, 'post aactual', 'breve resumen vlr', 'wspmomeod  doed oinond onoiedn', NULL, '2020-01-16 16:19:44', 'eraser', 1, 2),
(29, 'post aactual eiodne', 'breve resumen vlr idenoi', 'wspmomeod  doed o eodn eod inond onoiedn', NULL, '2020-01-16 16:20:55', 'eraser', 1, 3),
(30, 'post aactual eiodne enoni', 'breve resumen vlr idenoi eooe', 'wspmomeod  doed o eodn eod inond onoiedn einiude iedn', NULL, '2020-01-16 16:21:25', 'to_publish', 1, 4),
(32, 'iwbdo', 'pneodp', 'ionepd', NULL, '2020-01-16 16:26:27', 'to_publish', 1, 1),
(33, 'post de hoy', 'breve resumen vlmsowji oeid', 'pwniedi eiu  woinsown nwondi oiwnsiob uib2wu wisno oiwsio owsnwp obiodnw sooisnw onswnol sonoins wobiedio swiopnw o owoin', NULL, '2020-01-17 12:10:20', 'to_publish', 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` enum('active','no-active') DEFAULT NULL,
  `kind` enum('comun','admin') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `username`, `email`, `password`, `image`, `status`, `kind`, `created_at`) VALUES
(1, 'André', 'Joyo', 'ajoyo', 'ajoyo@gmail.com', 'contra', NULL, 'active', 'admin', '2020-01-07 17:50:00'),
(2, 'Franco', 'García', 'fgarcia', 'fgarcia@gmail.com', 'contra', NULL, 'active', 'admin', '2020-01-07 17:50:00'),
(3, 'n_comun1', 'a_comun1', 'comun1', 'comun1@gmail.com', 'contra', NULL, 'active', 'comun', '2020-01-07 17:52:00'),
(4, 'n_comun2', 'a_comun2', 'comun2', 'comun2@gmail.com', 'contra', NULL, 'active', 'comun', '2020-01-07 17:52:00'),
(5, 'n_comun3', 'a_comun3', 'comun3', 'comun3@gmail.com', 'contra', NULL, 'active', 'comun', '2020-01-07 17:53:00'),
(6, 'n_comun4', 'a_comun4', 'comun4', 'comun4@gmail.com', 'contra', NULL, 'active', 'comun', '2020-01-07 17:53:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indices de la tabla `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`);

--
-- Filtros para la tabla `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
