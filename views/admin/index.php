<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Main</title>
	<style>
		<?php require 'public/css/default.css'; ?>
	</style>
</head>
<body>
	<?php 
		session_start();
		if (isset($_SESSION['user'])) {
			require 'views/logout.php';
			echo "Usuario: ".$_SESSION['user'].'<br>';
		} else{
			require 'views/login.php';
		}
		var_dump($_SESSION);
	?>
	<h1>Administración del blog</h1>
	<?php
		require 'views/nav.php'
	?>
	<a href=<?php echo constant('URL').'admin/getPostsAdmin' ?> >Mostrar posts</a>
	<a href=<?php echo constant('URL').'admin/getCategoriesAdmin' ?> >Mostrar categorías</a>

	<form action=" <?php echo constant('URL'); ?>posts/addPost " method="POST">
		<h2>Crear post</h2>
		<p>
			<label for="title">Título</label><br>
			<input type="text" name="title" required>
		</p>
		<p>
			<label for="brief">Brief</label><br>
			<textarea rows="4" cols="50" name="brief" placeholder="Resumen" required></textarea>
		</p>
		<p>
			<label for="content">Content</label><br>
			<textarea rows="4" cols="50" name="content" placeholder="Contenido" required></textarea>
		</p>
		<p>
			<label for="content">Categoría</label><br>
			<select name="category" id="category" required>
	
			</select>		
		</p>
		<p>
			<select name="statusPost" id="statusPost">
				<option value="to_publish">Público</option>
				<option value="eraser">Borrador</option>
			</select>
		</p>
		<p>
			<input type="submit" value="Añadir post">
		</p>
	</form>
	<div id="posts">
		
	</div>
	<div id="categories">
		
	</div>
	<form action=" <?php echo constant('URL'); ?>categories/addCategory " method="POST">
		<p>
			<label for="content">Categoría</label><br>
			<input type="text" name="category" required>
		</p>
		<p>
			<input type="submit" value="Añadir categoría">
		</p>
	</form>
	<div id="comments">
		
	</div>
</body>
</html>