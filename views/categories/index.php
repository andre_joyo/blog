<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Categories</title>
	<style>
		<?php require 'public/css/default.css'; ?>
	</style>
</head>
<body>
	<?php 
		session_start();
		if (isset($_SESSION['user'])) {
			require 'views/logout.php';
			echo "Usuario: ".$_SESSION['user'].'<br>';
		} else{
			require 'views/login.php';
		}
		var_dump($_SESSION);
	?>
	<h1>Bienvenido a Categorías</h1>
	<?php 
		require 'views/nav.php';
		if (isset($_SESSION['user'])) {
			require 'views/admin.php';
		}
	?>
</body>
</html>