<?php
	class Categories extends Controller{
		function __construct(){
			parent::__construct();
			$this->view->render('categories/index');
			//echo "<p>Nuevo controlador categories</p>";
		}
		function addCategory(){
			$category=$_POST['category'];
			$insertion=$this->model->add(['category'=>$category]);
			if ($insertion) {
				$message='Inserción correcta';
			} else{
				$message='Inserción no completada';
			}
		}
	}
?>