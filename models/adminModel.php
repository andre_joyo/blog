<?php
	include_once 'models/post.php';
	include_once 'models/category.php';
	include_once 'models/comment.php';
	class adminModel extends Model{
		public function __construct(){
			parent::__construct();
		}
		
		public function getPosts(){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM post');
				while ($row=$query->fetch()) {
					$item=new Post();
					$item->title=$row['title'];
					$item->brief=$row['brief'];
					$item->content=$row['content'];
					$item->status=$row['status'];
					$item->id=$row['id'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function getCategories(){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM category');
				while ($row=$query->fetch()) {
					$item=new Category();
					$item->id=$row['id'];
					$item->name=$row['name'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function getPost($id){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM post WHERE id="'.$id.'"');
				while ($row=$query->fetch()) {
					$item=new Post();
					$item->title=$row['title'];
					$item->brief=$row['brief'];
					$item->content=$row['content'];
					$item->status=$row['status'];
					$item->id=$row['id'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function getCategory($id){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM category WHERE id="'.$id.'"');
				while ($row=$query->fetch()) {
					$item=new Category();
					$item->id=$row['id'];
					$item->name=$row['name'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function getComments($id){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM comment WHERE post_id="'.$id.'"');
				while ($row=$query->fetch()) {
					$item=new Comment();
					$item->id=$row['id'];
					$item->comment=$row['comment'];
					$item->created_at=$row['created_at'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function updatePost($id, $title,$brief, $content){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('UPDATE post SET title ="'. $title.'", brief = "'.$brief.'", content = "'.$content.'" WHERE id='.$id.';');
				while ($row=$query->fetch()) {
					$item=new Post();
					$item->title=$row['title'];
					$item->brief=$row['brief'];
					$item->content=$row['content'];
					$item->status=$row['status'];
					$item->id=$row['id'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function updateCategory($id, $name){//echo "insertar datos";
			$items=[];
			echo "id: ".$id."<br>";
			echo "name: ".$name."<br>";
			try{
				$query=$this->db->connect()->query('UPDATE category SET name ="'. $name.'" WHERE id='.$id.';');
				while ($row=$query->fetch()) {
					$item=new Category();
					$item->name=$row['name'];
					$item->id=$row['id'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function deletePost($id){//echo "insertar datos";
			try{
				$query=$this->db->connect()->query('DELETE FROM post WHERE id="'.$id.'"');
				//var_dump($items);
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function deleteCategory($id){//echo "insertar datos";
			try{
				$query=$this->db->connect()->query('DELETE FROM category WHERE id="'.$id.'"');
				//var_dump($items);
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
	}
?>